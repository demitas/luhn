#
# This code is based on Wikipedia 
#  [Luhn algorithm](https://en.wikipedia.org/wiki/Luhn_algorithm)
#

import sys

def checkLuhn(purportedCC):
    sum = int(purportedCC[-1])
    nDigits = len(purportedCC)
    parity = nDigits % 2
    for i in range(nDigits - 1) :
        digit = int(purportedCC[i])
        if i % 2 == parity:
            digit *= 2
        if digit > 9:
            digit = digit - 9 
        sum += digit
    return (sum % 10) == 0


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('usage: python luhn.py digits')
        exit()

    digits = sys.argv[1]
    print(f'{digits}: {checkLuhn(digits)}')
