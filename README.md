# luhn

Checksum with luhn number algorithm.

# Usage

```
python luhn.py digits
```

# License

The program is licensed under GPL v3.
The main code is based on the pseudocode in the artilce of Wikipedia:
  [Luhn algorithm](https://en.wikipedia.org/wiki/Luhn_algorithm)
